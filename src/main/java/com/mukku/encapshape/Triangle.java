/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.encapshape;

/**
 *
 * @author Acer
 */
public class Triangle {
    private double h;
    private double b;
    
    public Triangle(double h, double b){
        this.h = h;
        this.b = b;
    }
    
    public double calArea(){
        return 0.5*h*b;
    }
    
    public double getH(){
        return h;
    }
    
    public double getB(){
        return b;
    }
    
    public void setHB(double h, double b){
        if(h <= 0 || b <= 0){
            System.out.println("Error; High and base must more than 0!!");
            return;
        }
        this.h = h;
        this.b = b;
        
    }
}
