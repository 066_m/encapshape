/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.encapshape;

/**
 *
 * @author Acer
 */
public class TestSquare {
    public static void main(String[] args) {
        Square square = new Square(4);
        System.out.println("Area of square is " + square.calArea());
        square.setS(1.5);//square.s = 1.5;
        System.out.println("Area of square is " + square.calArea());
        square.setS(0);//square.s = 0;
        System.out.println("Area of square is " + square.calArea());
    }
}
