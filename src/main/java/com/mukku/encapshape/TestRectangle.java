/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.encapshape;

/**
 *
 * @author Acer
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(4,5);
        System.out.println("Area of rectangle (h = " + rectangle.getH() + ", w = " + rectangle.getW() + ") is " + rectangle.calArea());
        //rectangle.setH(5);//rectangle.h = 5;
        //rectangle.setW(0);//rectangle.w = 0;
        rectangle.setHW(5,0);
        System.out.println("Area of rectangle (h = " + rectangle.getH() + ", w = " + rectangle.getW() + ") is " + rectangle.calArea());
    }
}
